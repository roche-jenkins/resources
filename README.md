# Resources

**Requirements**

**Install Docker & Docker-Compose**
*Linux - Docker Engine*
- Ubuntu : 
https://docs.docker.com/engine/install/ubuntu/ 
- Fedora
https://docs.docker.com/engine/install/fedora/
- Debian
https://docs.docker.com/engine/install/debian/

- CentOS
https://docs.docker.com/engine/install/centos/

*Linux - Docker-Compose*
Choose a Linux : https://docs.docker.com/compose/install/

- *MacOS*
Docker ToolBox : https://docs.docker.com/toolbox/toolbox_install_mac/
Docker-Compose include in ToolBox.

- *Windows*
Docker ToolBox : https://docs.docker.com/toolbox/toolbox_install_windows/
Docker-Compose include in ToolBox.

**Install the laboratory**

*To up the service and create the lab*

`docker-compose up -d`
*Once the service is up, verify that the container was created correctly and enter your browser <IP_HOST>:8080*

*(Linux : localhost:8080)*
*(Windows / MacOS (Docker ToolBox) : <`docker-machine ip default`>:8080)*

*To stop the service*
`docker-compose stop`

*To restart the service*
`docker-compose restart`

**Another useful commands**

`docker ps` -> *Verify the containers are up*

`docker images` -> *Verify the images was built*

`docker rm {container_name/id}` -> *Remove a container*

`docker rmi {images_name}` -> *Remove a image*

`docker logs {container_name/id}` -> *Show the container logs*

`docker exec -ti {container_name/id}` -> *Get in the container*






